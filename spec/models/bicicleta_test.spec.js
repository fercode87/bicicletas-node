var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('testing bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useUnifiedTopology: true,useNewUrlParser:true});

        const db= mongoose.connection;
        db.on('error', console.error.bind(console, 'conection error'));
        db.once('open', function(){
            console.log('we are conected to testDB');
            done();
        });
    });
  


    afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
        if(err) console.log(err);
            done();
        });
    }); 

    describe('Bicicleta.createInstance', ()=>{
    it('creja instancia de bicicleta', ()=>{
        var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5,-54.5]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.5);  
    });
   
        });


    describe('bicicletas.allBicis',()=>{
    it('Probando si empieza vacia',(done)=>{
        Bicicleta.allBicis(function(err, bicis){
            expect(bicis.length).toBe(0);
            done();
         });
        });
    });

    describe('bicicletas.add',()=>{
        it('probando si agrega una',()=>{
            var aBici = new Bicicleta({code:1,color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici,function(err, bicis){
                expect(bicis.length).toBe(1);
                expect(bicis[0].code).toEqual(aBici.code);
                done();
            });
        });
    });
});


