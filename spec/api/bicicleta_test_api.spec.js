var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Testing Api GET', ()=>{
    it('Status 200',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.677688, -58.535555]);
        Bicicleta.add(a);
        request.get('http://localhost:3000/api/bicicletas',function(error, response, body){
            expect(response.statusCode).toBe(200);
        });
    });
});
describe('Testing Api Post', ()=>{
    it('Status 200',(done)=>{
    var headers = {'content-type':'application/json'};
    var aBici = '{"id":"1","color":"rojo","modelo":"urbana","lat":"-34","lng":"-58"}'
    request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/create',
        body: aBici
    }, function(error, response, body){
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(1).color).toBe('rojo');
        done();
});

    });
    });
