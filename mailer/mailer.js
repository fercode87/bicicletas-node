const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');

let mailconfig;
if(process.env.NODE_ENV==='production'){
    const options = {
        auth:{
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailconfig = sgTransport(options);
}
else{
    if(process.env.NODE_ENV=== 'stagging'){
        console.log('xxxxxxx');
        const options ={
            auth:{
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailconfig = sgTransport(options);
    }else{
        mailconfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pass
    }
}

/*const mailconfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'lew.quitzon54@ethereal.email',
        pass: 'DWQFwrzHjDXc1fGsrW'
    }
};*/

module.exports = nodemailer.createTransport(mailconfig);